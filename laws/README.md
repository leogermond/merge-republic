This directory contains "laws", which are configuration files that are interpreted by the implementation.

The idea is that laws will change more frequently in content, but less frequently in "shape", so that we can keep them in mostly the same format for a long time.

# Law books

* Constitution: This represents the most fundamental info for the judge: which project is being manipulated, how to know when a merge should actually happen (as opposed to a dry-run, which is the default)
* Vote: List of constituents, and vote modalities
