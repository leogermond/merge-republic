# MergeRepublic

![Drapeau de la MergeRepublic](flag.png){width=100px}

Bienvenue dans la MergeRepublic, où l'esprit de la participation démocratique se lance dans un voyage à travers le code et la communauté ! Nous créons un espace où les mondes techniques et non-techniques fusionnent, favorisant un environnement inclusif où chacun façonne l'évolution de notre logiciel.

## Pas seulement du code

MergeRepublic ne concerne pas seulement le code ; il s'agit de collaboration, de perspective, et de diversité d'opinions. Au cœur de notre approche repose un processus dirigé par la communauté où chaque proposition de changement du code, chaque 'Proposition de Fusion' (*Merge Request*), est soumise au vote de la communauté. Ici, vous n'avez pas besoin d'être un codeur chevronné pour faire une différence. Si vous pouvez apprécier un bon code, repérer des améliorations potentielles, ou simplement avoir une idée pour une nouvelle fonctionnalité cool, MergeRepublic est l'endroit pour vous.

## Que contient ce dépot ?

Ce dépot contient un ensemble de script qui, associés a une configuration de pipeline GitLab, forment la preuve de concept d'un project qui
est automatiquement géré au travers des votes de la communauté. Tout changement du code du dépot doit etre soumis au vote (en utilisant les emoji de pouces) et tout vote qui
réussit entraine une mise a jour inconditionnelle du code du dépot.

Bien sur, puisqu'il s'agit d'une preuve de concept, elle se limite au strict minimum: le code dans le dépot est uniquement
le code servant a implémenter cette preuve de concept elle meme.

Cela pourrait sembler trés circulaire comme fonctionnement, mais c'est sans compter sur la possibilité pour
n'importe qui de faire une 'bifurcation' (*fork*) de ce dépot dans leur propre projet, intégrant ainsi un systeme de gouvernance
ouverte et démocratique au prix d'un léger travail d'adaptation.

## Voir le Processus en Action

Pour voir le fonctionnement dans le détail, rendez vous dans la section des *Merge Requests* (MR) et vous pourrez voir comment les modifications
sont apportées: une MR est créée, les Gouvernautes votent pour ou contre, et les modifications sont finalement aportées a la branche principale
du dépot par un bot (appelé Juge dans la MergeRepublic) si le vote réussit.

C'est un mécanisme simple et transparent de décision, qui n'implique aucun "dictateur bienveillant".

## Devenez un Gouvernaute

Nous appelons affectueusement nos membres 'Gouvernautes', un terme qui incarne l'essence de notre vision. Un Gouvernaute est un pionnier, se lançant dans l'exploration de la gouvernance participative dans le domaine du développement de logiciels. Ils naviguent à travers des discussions, des débats et des décisions, façonnant et guidant le chemin du projet. Le nom représente le pouvoir entre les mains de notre communauté, indiquant que chaque participant n'est pas seulement un spectateur mais un navigateur actif dirigeant la direction de notre logiciel.

MergeRepublic redéfinit le développement de logiciels comme un processus véritablement démocratique, permettant à chacun d'entre nous d'avoir son mot à dire dans les outils numériques que nous utilisons tous les jours.

Rejoignez MergeRepublic aujourd'hui, devenez un Gouvernaute, et remodelons ensemble le paysage du développement de logiciels, un vote à la fois!

### Comment Participer

Pour participer a MergeRepublic, vous devez devenir Gouvernaute, pour cela rien de plus simple: ouvrez soit une requete de participation pour etre ajouté a la liste des votants, ou bien ajoutez vous vous-meme en modifiant [laws/vote.toml](laws/vote.toml).
Les demandes de participation sont validées manuellement pour le moment, donc vous aurez besoin d'attendre quelques heures :)

## Fait pour bifurquer

MergeRepublic incarne l'esprit d'évolution. Dans le monde du développement de logiciels, 'bifurquer' (*fork*) est un terme familier - une divergence, un nouveau chemin pris à partir d'un existant. Mais dans MergeRepublic, la bifurcation prend une toute nouvelle dimension. Il ne s'agit pas seulement de code ; il s'agit de l'essence de notre processus démocratique et participatif.

Imaginez un arbre. À partir d'une seule graine, il grandit, se ramifiant dans plusieurs directions. Chaque branche est unique, mais partage les mêmes racines. MergeRepublic est cette graine, et chaque bifurcation de notre projet devient une nouvelle branche de cet arbre en constante évolution. Chaque nouvelle branche - ou plutôt, chaque nouveau 'gouvernement' - porte en elle le potentiel de voies de développement diverses, de méthodologies uniques et de pratiques communautaires sur mesure. En un sens, chaque bifurcation crée sa propre "République", avec son propre ensemble de Gouvernautes, dirigeant l'évolution de leur projet de logiciel.

## Résoudre les problèmes courants des logiciels libres et open source (FOSS)

Cet aspect transformateur de la bifurcation dans MergeRepublic introduit une  solution théorique aux problèmes de nombreux projets logiciels libres et open source (FOSS). Combien de fois les projets FOSS ont vu une grande idée se perdre au milieu d'une mer d'opinions, ou bien le projet souffrir de la 'tyrannie de la majorité' ? Combien de fois la direction d'un projet s'écarte-t-elle de ce qu'un groupe d'utilisateurs ou de développeurs souhaitait ?

Dans MergeRepublic, ces voix minoritaires peuvent faire bifurquer le projet, emportant le code et le processus de prise de décision avec eux. Ils peuvent proposer leurs propres 'Propositions de Fusion', développer leur communauté, et modeler le logiciel selon leur vision. Le pouvoir de la prise de décision collective s'étend maintenant à l'évolution du processus de prise de décision lui-même. C'est une belle construction théorique - une solution démocratique à un problème démocratique.

MergeRepublic fait plus que permettre le codage collaboratif ; elle révolutionne notre perception du concept de 'communauté' dans le FOSS. La bifurcation n'est plus une action de dernier recours lorsque des désaccords surviennent ; elle est une partie naturelle et célébrée du processus de développement. Chaque bifurcation, chaque nouvelle branche sur l'arbre, est un témoignage de la diversité et de l'innovation qui prospèrent dans des environnements véritablement démocratiques.

En vous engageant dans MergeRepublic, vous ne contribuez pas simplement à un projet de logiciel. Vous participez à une expérience révolutionnaire de gouvernance démocratique dans le domaine du FOSS. Vous embrassez le potentiel pour des chemins d'évolution infinis. Vous devenez un Gouvernaute - un navigateur sur ce voyage passionnant.

MergeRepublic n'est pas seulement un projet ; c'est une provocation, une invitation à participer, à choisir, à débattre. C'est un appel à l'action - pour façonner et être façonné par la communauté. Bienvenue à l'évolution. Bienvenue dans la MergeRepublic.

# Aspects techniques

D'un point de vue technique, MergeRepublic utilise GitLab CI pour vérifier automatiquement les conditions de vote et appliquer les actions de fusion.
Il est conçu pour gérer à la fois les travaux déclenchés par un push et les travaux planifiés. Le protocole de vote de la plateforme est lui-même ouvert aux mises à jour par le biais de demandes de fusion et de votes. Cela permet au système d'évoluer dynamiquement avec les besoins et les souhaits de la communauté.
