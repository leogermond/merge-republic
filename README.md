# Merge Republic

![MergeRepublic Flag](flag.png){width=100px}

[Version Francaise](README.fr.md)

Welcome to MergeRepublic, where the spirit of democratic participation embarks on a journey through code and community! We're creating a space where technical and non-technical worlds merge, fostering an inclusive environment where every opinion shapes the evolution of our software.

## What this repository contains

This repository contains a set of scripts that are used to implement a fully working proof-of-concept for a project that is
managed automatically by votes of the community. Any change to the source must go through a vote, and any vote that passes is
applied automatically, no questions asked.

Of course since this is a proof-of-concept, it does not contain much apart from the voting and decision algorithms themselves,
but this in itself is already something new, and useful, since anyone can fork and then adapt this repository to build their
own community-driven repository governance on top of it.

## See the Process at Play

To see this process working in details, go into the Merge Requests (MR) tab, there you'll be able to see how modifications are
applied: a MR is created, community votes on it using the thumbs up / thumbs down emoji, and then if the vote passes, a bot (called
*Judge* in MergeRepublic) merges the modification onto the main branch.

A simple, transparent, decision process, that does not require any "benevolent dictator" to work.

## Not just about code

MergeRepublic is not just about code; it's about collaboration, perspective, and the diversity of insights. At its core, our approach rests on a community-driven process where each proposed change to the code, every 'Merge Proposal', is subject to community voting. Here, you don't need to be a seasoned coder to make a difference. If you can appreciate good code, spot potential improvements, or simply have an idea for a cool new feature, MergeRepublic is the place for you.

## Become a Governaut

We affectionately call our members 'Governauts', a term embodying the essence of our vision. A Governaut is a pioneer, embarking on the exploration of participative governance within the realm of software development. They navigate through discussions, debates, and decisions, shaping and guiding the path of the project. The name represents the power in the hands of our community, indicating that every participant is not just a spectator but an active navigator steering the direction of our software.

MergeRepublic redefines software development as a truly democratic process, empowering each one of us to have a say in the digital tools we use every day.

Join MergeRepublic today, become a Governaut, and let's reshape the software development landscape together, one vote at a time!

### How to join MergeRepublic

In order to participate, simply ask to be added to the pool of voters through an issue, or add yourself to it through the modification of (laws/vote.toml)[laws/vote.toml]!
This list is updated manually, so it may take some hours :)

# Made to be forked

MergeRepublic embodies the spirit of evolution. In the world of software development, 'forking' is a familiar term - a divergence, a new path taken from an existing one. But in the MergeRepublic, forking takes on an entirely new dimension. It isn't just about code; it's about the essence of our democratic, participative process.

Picture a tree. From a single seed, it grows, branching out in multiple directions. Each branch is unique, yet shares the same roots. MergeRepublic is that seed, and every fork of our project becomes a new branch of this ever-evolving tree. Each new branch - or rather, each new 'government' - carries the potential for diverse development pathways, unique methodologies, and bespoke community practices. In a sense, every fork creates its own "Republic," complete with its own set of Governauts, steering the evolution of their software project.

## Helping solve Free Open-Source Software (FOSS) common issues

This transformative aspect of forking in MergeRepublic introduces a theoretical elegance to the solution of many Free Open-Source Software (FOSS) issues. How many times have FOSS projects seen a great idea lost amidst a sea of opinions, only to see the project suffer from the 'tyranny of the majority'? How often does the direction of a project deviate from what a group of users or developers desired?

In MergeRepublic, those minority voices can fork the project, taking the code and the decision-making process with them. They can propose their own 'Merge Proposals,' foster their community, and shape the software according to their vision. The power of collective decision-making now extends to the evolution of the decision-making process itself. It’s a beautiful theoretical construct - a democratic solution to a democratic problem.

MergeRepublic does more than enable collaborative coding; it revolutionizes how we perceive the concept of 'community' in FOSS. Forking is no longer a last-resort action when disagreements arise; it's a natural, celebrated part of the development process. Each fork, each new branch on the tree, is a testament to the diversity and innovation that thrives in truly democratic environments.

By engaging with MergeRepublic, you're not just contributing to a software project. You are partaking in a groundbreaking experiment in democratic governance in the realm of FOSS. You are embracing the potential for infinite evolution paths. You are becoming a Governaut - a navigator on this exciting journey.

MergeRepublic isn't just a project; it's a provocation, an invitation to participate, choose, and debate. It is a call to action - to shape and to be shaped by the community. Welcome to the evolution. Welcome to MergeRepublic.

# Technical aspects

From a technical standpoint, MergeRepublic uses GitLab CI for automatic checking of voting conditions and application of merge actions. It’s designed to handle both push-triggered and scheduled jobs. The platform's voting protocol is open to updates through merge requests and voting itself, allowing the system to evolve dynamically with the needs and wishes of the community.
