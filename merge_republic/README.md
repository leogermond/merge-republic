This directory contains two things:

- The "framework" used by the merge republic to function: building blocks for the bots
- The entry point to run the commands: high-level CLI tools that are called directly by the bots, or by users for maintenance and setup.
