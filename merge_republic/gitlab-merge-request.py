import os
import argparse
from pathlib import Path
import toml
import json
import gitlab
import logging

logging.basicConfig(level=logging.DEBUG)

from merge_republic import law

log = logging.getLogger("GitLab Merge Request")

judge = law.MergeRepublicJudge()


def pretty_print(obj):
    if isinstance(obj, gitlab.v4.objects.merge_requests.ProjectMergeRequest):
        return f"{obj.title!r} #{obj.iid}"
    else:
        raise Exception(f"not supported for objects of type {obj.__class__.__name__}")


class GitlabMergeRequestApp:
    def __init__(self):
        token = os.environ.get("GITLAB_TOKEN")
        can_merge = os.environ.get("CAN_MERGE", False)
        self.dry_run = not token or not can_merge
        if self.dry_run:
            # public access...
            self.gl = gitlab.Gitlab()
        else:
            self.gl = gitlab.Gitlab(private_token=token)
            # Not necessary, but validates correct auth
            self.gl.auth()
        self.project = self.gl.projects.get(law.constitution["Gitlab_Project"])

    def list(self, args):
        for mr in judge.only_mergeable_project_mr(self.project):
            if args.json:
                json.dumps(mr)
            else:
                print(mr)
                print(judge.count_votes_mr(mr))

    def merge(self, args):
        for mr in judge.only_mergeable_project_mr(self.project):
            if judge.can_merge_mr(mr):
                log.info(f"merging {pretty_print(mr)}")
                if self.dry_run:
                    log.warning("dry run merge")
                else:
                    mr.merge(merge_when_pipeline_succeeds=True)

    @property
    def commands(self):
        return [
            n
            for n in dir(self)
            if n != "commands" and not n.startswith("_") and callable(getattr(self, n))
        ]


if __name__ == "__main__":
    app = GitlabMergeRequestApp()
    ap = argparse.ArgumentParser()
    ap.add_argument("command", choices=app.commands)
    ap.add_argument("--json", action="store_true")
    args = ap.parse_args()

    getattr(app, args.command)(args)
