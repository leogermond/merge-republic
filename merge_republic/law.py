import os
import argparse
from pathlib import Path
import toml
import json
import gitlab
import logging

log = logging.getLogger("Law")


ROOT = Path(__file__).resolve().parents[1]
LAWS = ROOT / "laws"


# Global
constitution = None

with open(LAWS / "constitution.toml") as constitution_fd:
    constitution = toml.load(constitution_fd)


class MergeRepublicJudge:
    """
    The judges interpret the laws, if the law is a GPS, the judge the car driver.
    """

    def __init__(self):
        def toml_parse(path):
            with open(path) as fd:
                return toml.load(fd)

        self.laws = {
            str(l.relative_to(LAWS).with_suffix("")): toml_parse(l)
            for l in LAWS.glob("**/*.toml")
        }

    def only_mergeable_project_mr(self, project):
        for mr in project.mergerequests.list():
            if mr.state != "merged":
                yield mr

    def count_votes_mr(self, mr):
        tally = {t: 0 for t in self.laws["vote"]["Vote_Types"]}
        approving = []
        for approval in mr.approvals.get().approved_by:
            approver = approval["user"]["username"]
            if approver in self.laws["vote"]["Can_Vote"]:
                approving.append(approver)

        for emoji in mr.awardemojis.list():
            username = emoji.user["username"]
            if username in self.laws["vote"]["Can_Vote"] and emoji.name in tally:
                if (
                    username not in approving
                    or emoji.name != self.laws["vote"]["Approval_Interpreted_As"]
                ):
                    tally[emoji.name] += 1

        tally[self.laws["vote"]["Approval_Interpreted_As"]] += len(approving)
        return tally

    def can_merge_mr(self, mr):
        tally = self.count_votes_mr(mr)
        return tally["thumbsup"] >= 1 and tally["thumbsdown"] == 0
